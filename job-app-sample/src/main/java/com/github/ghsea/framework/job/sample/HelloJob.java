package com.github.ghsea.framework.job.sample;

import java.util.concurrent.TimeUnit;

import com.github.ghsea.framework.job.client.Job;
import com.github.ghsea.framework.job.client.JobExecutionContext;

public class HelloJob implements Job {

	public void execute(JobExecutionContext exeCtx) {
		System.out.println("Say Hello to Sea-Job server");
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
