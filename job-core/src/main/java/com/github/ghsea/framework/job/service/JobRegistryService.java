package com.github.ghsea.framework.job.service;

import com.github.ghsea.framework.job.common.TriggerMetadata;
import com.github.ghsea.framework.job.common.exception.JobRegistryException;

public interface JobRegistryService {

	void register(TriggerMetadata trigger) throws JobRegistryException;
	
	void update(TriggerMetadata trigger);
}
