package com.github.ghsea.framework.job.support;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.github.ghsea.framework.job.exception.ConfigurationException;
import com.google.common.base.Strings;

public class ServerConfig {

	private Properties config = null;

	private static final String PRO_ZKSERVERS = "zkServers";

	private static final String PRO_PORT = "port";

	private static final String BASE_ACCESS_URL = "baseAccessUrl";

	public ServerConfig(String configFileName) {
		try {
			config = PropertiesLoaderUtils.loadAllProperties(configFileName);
		} catch (IOException e) {
			throw new ConfigurationException(e);
		}
	}

	public String getZkServers() {
		String zkServers = config.getProperty(PRO_ZKSERVERS);
		if (Strings.isNullOrEmpty(zkServers)) {
			throw new ConfigurationException("'zkServers' is not configured");
		}

		return zkServers;
	}

	public int getPort() {
		String port = config.getProperty(PRO_PORT);
		if (Strings.isNullOrEmpty(port)) {
			throw new ConfigurationException("'zkServers' is not configured");
		}

		return Integer.parseInt(port.trim());
	}

	public String getBaseAccessUrl() {
		String baseAccessUrl = config.getProperty(BASE_ACCESS_URL);
		if (Strings.isNullOrEmpty(baseAccessUrl)) {
			throw new ConfigurationException("'baseAccessUrl' is not configured");
		}

		return baseAccessUrl;
	}

}
