package com.github.ghsea.framework.job.module;

import java.io.Serializable;
import java.util.Date;

import com.github.ghsea.framework.job.core.JobKey;
import com.github.ghsea.framework.job.core.JobServerKey;

public class JobAssignment implements Serializable {

	private static final long serialVersionUID = -8473669767201305339L;

	private Long id;

	private JobServerKey serverKey;

	private JobKey jobKey;

	private Date createTime;

	private Date updateTime;

	private Date assignmentTime;

	private int isDelete;

	// public JobAssignment(JobServerKey serverKey, JobKey jobKey) {
	// this.serverKey = serverKey;
	// this.jobKey = jobKey;
	// }

	public JobServerKey getServerKey() {
		return serverKey;
	}

	public JobKey getJobKey() {
		return jobKey;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setServerKey(JobServerKey serverKey) {
		this.serverKey = serverKey;
	}

	public void setJobKey(JobKey jobKey) {
		this.jobKey = jobKey;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getAssignmentTime() {
		return assignmentTime;
	}

	public void setAssignmentTime(Date assignmentTime) {
		this.assignmentTime = assignmentTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "JobAssignment [id=" + id + ", serverKey=" + serverKey + ", jobKey=" + jobKey + ", createTime="
				+ createTime + ", updateTime=" + updateTime + ", assignmentTime=" + assignmentTime + ", isDelete="
				+ isDelete + "]";
	}

}
