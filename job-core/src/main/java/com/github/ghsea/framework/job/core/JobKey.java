package com.github.ghsea.framework.job.core;

/**
 * 
 * @author Administrator
 *
 */
public class JobKey {
	/**
	 * 应用名，用作JobDetail的group名
	 */
	private String appName;

	private String jobName;

	public JobKey() {

	}

	public JobKey(String appName, String jobName) {
		this.appName = appName;
		this.jobName = jobName;
	}

	public String getAppName() {
		return appName;
	}

	public String getJobName() {
		return jobName;
	}

}
