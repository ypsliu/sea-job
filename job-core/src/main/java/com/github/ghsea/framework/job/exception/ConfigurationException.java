package com.github.ghsea.framework.job.exception;

public class ConfigurationException extends RuntimeException {
	
	private static final long serialVersionUID = 4059355134875520884L;

	public ConfigurationException(String msg) {
		super(msg);
	}

	public ConfigurationException(Exception ex) {
		super(ex);
	}
}
