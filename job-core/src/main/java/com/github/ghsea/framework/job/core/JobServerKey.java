package com.github.ghsea.framework.job.core;

import java.io.Serializable;

public class JobServerKey implements Serializable {

	private static final long serialVersionUID = -6216336099069743223L;

	private String ip;

	private int port;

	public JobServerKey() {

	}

	public JobServerKey(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	@Override
	public int hashCode() {
		return ip.hashCode() + port;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof JobServerKey)) {
			return false;
		}

		JobServerKey otherKey = (JobServerKey) other;
		return otherKey.getIp().equals(this.getIp()) && otherKey.getPort() == this.getPort();
	}

	@Override
	public String toString() {
		return ip + ":" + port;
	}

}
