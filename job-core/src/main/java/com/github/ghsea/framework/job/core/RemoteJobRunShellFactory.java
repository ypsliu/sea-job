package com.github.ghsea.framework.job.core;

import org.quartz.Scheduler;
import org.quartz.SchedulerConfigException;
import org.quartz.SchedulerException;
import org.quartz.core.JobRunShell;
import org.quartz.core.JobRunShellFactory;
import org.quartz.core.RemoteJobRunShell;
import org.quartz.spi.TriggerFiredBundle;

public class RemoteJobRunShellFactory implements JobRunShellFactory {

	private Scheduler scheduler;

	public RemoteJobRunShellFactory() {
	}

	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * 
	 * Interface.
	 * 
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

	/**
	 * <p>
	 * Initialize the factory, providing a handle to the <code>Scheduler</code> that
	 * should be made available within the <code>JobRunShell</code> and the
	 * <code>JobExecutionContext</code> s within it, and a handle to the
	 * <code>SchedulingContext</code> that the shell will use in its own operations
	 * with the <code>JobStore</code>.
	 * </p>
	 */
	public void initialize(Scheduler sched) throws SchedulerConfigException {
		this.scheduler = sched;
	}

	public JobRunShell createJobRunShell(TriggerFiredBundle bundle) throws SchedulerException {
		return new RemoteJobRunShell(scheduler, bundle);
	}

}
