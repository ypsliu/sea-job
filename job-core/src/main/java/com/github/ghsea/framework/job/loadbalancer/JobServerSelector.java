package com.github.ghsea.framework.job.loadbalancer;

import com.github.ghsea.framework.job.core.JobServerKey;

public interface JobServerSelector {

	/**
	 * 为Job选择一个JobServer
	 * @param appName
	 * @param jobName
	 * @return
	 */
	JobServerKey select(String appName,String jobName);
}
