package com.github.ghsea.framework.job.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.github.ghsea.framework.job.core.JobKey;
import com.github.ghsea.framework.job.core.JobServerKey;
import com.github.ghsea.framework.job.module.JobAssignment;

@MapperScan
public interface JobAssignmentDao {

	/**
	 * 
	 * @return key=Job Server Ip,value=为该IP的server分配的Job个数
	 */
	@MapKey("server_key")
	Map<String, Integer> countByJobServer();

	void insert(@Param("entity") JobAssignment assignment);

	/**
	 * 根据主键更新
	 * 
	 * @param assignment
	 * @return
	 */
	int updateByPk(@Param("entity")JobAssignment assignment);

	/**
	 * 查找在lastScanTime时间之后新创建的JobAssignment
	 * 
	 * @param serverKey
	 * @param lastScanTime
	 * @return
	 */
	List<JobAssignment> findNeedAssign(@Param("serverKey")JobServerKey serverKey,@Param("lastScanTime")Date lastScanTime);

	JobAssignment findByJobKey(JobKey jobKey);

	/**
	 * 根据业务主键查找
	 * 
	 * @param serverKey
	 * @param jobKey
	 * @return
	 */
	JobAssignment get(JobServerKey serverKey, JobKey jobKey);

}
