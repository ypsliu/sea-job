package com.github.ghsea.framework.job.api.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.ghsea.framework.job.api.JobRegistry;
import com.github.ghsea.framework.job.common.AuthorityInfo;
import com.github.ghsea.framework.job.common.TriggerMetadata;
import com.github.ghsea.framework.job.common.exception.JobRegistryException;
import com.github.ghsea.framework.job.service.JobRegistryService;

@Component(value = "jobRegistry")
public class JobRegistryImpl implements JobRegistry {

	@Autowired
	private JobRegistryService registryService;

	private Logger log = LoggerFactory.getLogger(JobRegistryImpl.class);

	@Override
	public void register(AuthorityInfo authority, TriggerMetadata trigger) throws JobRegistryException {
		try {
			registryService.register(trigger);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw new JobRegistryException(ex.getMessage());
		}

	}

}
