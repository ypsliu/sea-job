package com.github.ghsea.framework.job.api.impl;

import org.springframework.stereotype.Component;

import com.github.ghsea.framework.job.api.JobExecuteResultReport;
import com.github.ghsea.framework.job.common.JobExecuteResult;

@Component(value = "jobExecuteResultReport")
public class JobExecuteResultReportImpl implements JobExecuteResultReport {

	@Override
	public void report(JobExecuteResult ret) {
		System.out.println("JobServer received report:" + ret);

	}

}
