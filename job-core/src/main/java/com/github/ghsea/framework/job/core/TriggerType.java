package com.github.ghsea.framework.job.core;

public enum TriggerType {

	CRON_TRIGGER,
	
	SIMPLE_TRIGGER;
}
