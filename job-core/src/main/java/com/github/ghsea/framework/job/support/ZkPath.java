package com.github.ghsea.framework.job.support;

import java.util.List;

import org.apache.curator.utils.ZKPaths;

import com.github.ghsea.framework.job.core.JobServerKey;
import com.google.common.base.Splitter;

public class ZkPath {

	public static final String ZK_PATH_ROOT = "/sea-job";

	public static final String ZK_PATH_SERVER = "/servers";

	public static final String ZK_PATH_CLIENT = "/clients";

	private ZkPath() {

	}

	/**
	 * /sea-job/servers
	 * 
	 * @return
	 */
	public static String getPathOfServers() {
		return ZKPaths.makePath(ZK_PATH_ROOT, ZK_PATH_SERVER);
	}

	public static String getPathOfClients() {
		return ZKPaths.makePath(ZK_PATH_ROOT, ZK_PATH_CLIENT);
	}

	public static JobServerKey node2ServerKey(String serverPath) {
		List<String> ipAndPort = Splitter.on(":").trimResults().splitToList(serverPath);
		return new JobServerKey(ipAndPort.get(0), Integer.parseInt(ipAndPort.get(1)));
	}

}
