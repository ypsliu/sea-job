package com.github.ghsea.framework.job.dao;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.github.ghsea.framework.job.common.TriggerMetadata;
import com.github.ghsea.framework.job.core.JobKey;

@MapperScan
public interface TriggerMetadataDao {

	void insert(TriggerMetadata triggerMeta);

	void update(TriggerMetadata triggerMeta);

	
	TriggerMetadata findByJobKey(@Param("jobKey")JobKey jobKey);

}
