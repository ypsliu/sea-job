package com.github.ghsea.framework.job.core;

import org.quartz.JobDataMap;

public class JobDataMapWrapper extends JobDataMap {

	private final static String TARGET_CLASS = "JOB_CLASS";

	private final static String TARGET_METHOD = "JOB_METHOD";

	public JobDataMapWrapper(JobDataMap dataMap) {
		super(dataMap);
	}

	public JobDataMapWrapper() {
		super();
	}

	public void putJobClass(String jobName) {
		if (super.getString(TARGET_CLASS) != null) {
			throw new IllegalArgumentException("A key named" + TARGET_CLASS + "  exists");

		}

		super.put(TARGET_CLASS, jobName);
	}

	public void putJobMethod(String methodName) {
		if (super.getString(TARGET_METHOD) != null) {
			throw new IllegalArgumentException("A key named" + TARGET_METHOD + " exists");

		}

		super.put(TARGET_METHOD, methodName);
	}

	public String getJobClass() {
		return super.getString(TARGET_CLASS);
	}

	public String getJobMethod() {
		return super.getString(TARGET_METHOD);
	}
}
