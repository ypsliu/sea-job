package com.github.ghsea.framework.job.core;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.spi.OperableTrigger;

import com.github.ghsea.framework.job.common.JobExecutorParams;
import com.github.ghsea.framework.job.core.client.JobExecutorClient;

/**
 * 
 * @author guhai
 *
 */
public class StubJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDetail jobDetail = context.getJobDetail();

		OperableTrigger trigger = (OperableTrigger) context.getTrigger();

		JobDataMap dataMap = jobDetail.getJobDataMap();

		JobDataMapWrapper dataMapWrapper = new JobDataMapWrapper(dataMap);
		String jobClass = dataMapWrapper.getJobClass();
		String jobMethod = dataMapWrapper.getJobMethod();
		String appName = jobDetail.getKey().getGroup();

		JobExecutorClient executorClient = JobExecutorClient.getInstance(appName);
		JobExecutorParams exeParams=new JobExecutorParams();
		exeParams.setTargetClass(jobClass);
		executorClient.execute(exeParams);
		
		// send the execution request to client

		CompletedExecutionInstruction instCode = trigger.executionComplete(context, null);
	}

}
