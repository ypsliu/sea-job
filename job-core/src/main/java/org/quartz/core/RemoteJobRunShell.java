package org.quartz.core;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.spi.OperableTrigger;
import org.quartz.spi.TriggerFiredBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 将job.execute()代理至客户端
 * 
 * @author guhai
 *
 */
public class RemoteJobRunShell extends JobRunShell {

	private final Logger log = LoggerFactory.getLogger(getClass());

	public RemoteJobRunShell(Scheduler scheduler, TriggerFiredBundle bndle) {
		super(scheduler, bndle);
	}

	@Override
	public void run() {
		qs.addInternalSchedulerListener(this);

		try {

			Job job = jec.getJobInstance();
			job.execute(jec);
			OperableTrigger trigger = (OperableTrigger) jec.getTrigger();
			JobDetail jobDetail = jec.getJobDetail();

			CompletedExecutionInstruction instCode = trigger.executionComplete(super.jec, null);

			// TODO 这个方法用于清理已经执行的trigger,qrtz_fired_triggers表
			 qs.notifyJobStoreJobComplete(trigger, jobDetail, instCode);
		}
		// TODO send the job execution request to client
		catch (Exception ex) {
			// TODO
			log.error(ex.getMessage(), ex);
		} finally

		{
			qs.removeInternalSchedulerListener(this);
		}
	}

}
