create table job_assignment(
  id bigint not null auto_increment,
  ip VARCHAR(15) NOT NULL,
  port int not null,
  app_name VARCHAR(30) NOT NULL,
  job_name VARCHAR(30) NOT NULL,  
  create_time datetime not null,
  update_time  datetime,
  assignment_time datetime,
  is_delete tinyint default 0,
  PRIMARY KEY (id)
);

create table Trigger_Metadata(
  id bigint not null auto_increment,
  app_name VARCHAR(30) NOT NULL,
  job_name VARCHAR(30) NOT NULL,  
  job_class VARCHAR(50) NOT NULL,
  cron_expression VARCHAR(30) NULL, 
  repeat_interval_in_ms int,
  repeat_count bigint,
  shard_count int,
  version int not null default 0,
  description VARCHAR(100) ,
  create_time datetime not null,
  update_time  datetime,
  disabled tinyint default 0,
  is_delete tinyint default 0,
  PRIMARY KEY (id)
);