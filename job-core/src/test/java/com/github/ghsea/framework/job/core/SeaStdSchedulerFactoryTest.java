package com.github.ghsea.framework.job.core;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.ghsea.framework.job.common.TriggerMetadata;
import com.github.ghsea.framework.job.support.TriggerMetadataUtil;

public class SeaStdSchedulerFactoryTest {
	public void run() throws Exception {
		String group = "group5";
		Logger log = LoggerFactory.getLogger(SeaStdSchedulerFactoryTest.class);

		log.info("------- Initializing ----------------------");

		// First we must get a reference to a scheduler
		SchedulerFactory sf = new SeaStdSchedulerFactory("quartz-demo.properties");
		// SchedulerFactory sf = new SeaStdSchedulerFactory();
		Scheduler sched = sf.getScheduler();

		TriggerMetadata jobInfo = new TriggerMetadata();
		jobInfo.setAppName("groupon-task");
		jobInfo.setJobName("TestJob");
		jobInfo.setCronExpression("0/20 * * * * ?");
		jobInfo.setJobClass("com.github.ghsea.framework.test.AdvertiseTask");
		TriggerMetadataUtil jobInfoUtil = new TriggerMetadataUtil(jobInfo);
		JobDetail job = jobInfoUtil.buildJobDetail();
		Trigger trigger = jobInfoUtil.buildTrigger();

		log.info("------- Initialization Complete -----------");

		// computer a time that is on the next round minute
		// Date runTime = DateBuilder.evenMinuteDate(new Date());

		log.info("------- Scheduling Job  -------------------");
		/*
		 * JobDetail job = JobBuilder.newJob(StubJob.class).withIdentity("cron-job",
		 * group).build();
		 * 
		 * CronTrigger trigger =
		 * TriggerBuilder.newTrigger().withIdentity("cron-trigger", group)
		 * .withSchedule(CronScheduleBuilder.cronSchedule("0/20 * * * * ?")).build();
		 */
		// Tell quartz to schedule the job using our trigger
		sched.scheduleJob(job, trigger);
		// log.info(job.getKey() + " will run at: " + runTime);

		// Start up the scheduler (nothing can actually run until the
		// scheduler has been started)
		sched.start();

		log.info("------- Started Scheduler -----------------");

		// wait long enough so that the scheduler as an opportunity to
		// run the job!
		log.info("------- Waiting 65 seconds... -------------");
		try {
			// wait 65 seconds to show job
			Thread.sleep(65L * 1000L);
			// executing...
		} catch (Exception e) {
			//
		}

		// shut down the scheduler
		log.info("------- Shutting Down ---------------------");
		sched.shutdown(true);
		log.info("------- Shutdown Complete -----------------");
	}

	public static void main(String[] args) throws Exception {

		SeaStdSchedulerFactoryTest example = new SeaStdSchedulerFactoryTest();
		example.run();

	}
}
