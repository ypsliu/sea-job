package com.github.ghsea.framework.job.common;

import java.io.Serializable;
import java.util.Date;

import org.apache.curator.shaded.com.google.common.base.Strings;

import com.github.ghsea.framework.job.common.exception.JobConfigurationException;

public class TriggerMetadata implements Serializable {

	private static final long serialVersionUID = 57388280627892470L;

	private String appName;
	private String jobName;

	// used by CronTrigger
	private String cronExpression;

	// ===================== used by SimpleTrigger
	private Integer repeatIntervalInMs;

	private Integer repeatCount;
	//

	/**
	 * 客户端job类
	 */
	private String jobClass;

	/**
	 * 最大分片数
	 */
	private int shardCount = 1;

	private int version = 0;

	private int disabled = 0;

	private String description;

	// TODO 不暴露给客户端
	private Date createTime;

	// TODO 不暴露给客户端
	private Date updateTime;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getRepeatIntervalInMs() {
		return repeatIntervalInMs;
	}

	public void setRepeatIntervalInMs(Integer repeatIntervalInMs) {
		this.repeatIntervalInMs = repeatIntervalInMs;
	}

	public Integer getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public int getShardCount() {
		return shardCount;
	}

	public void setShardCount(int shardCount) {
		this.shardCount = shardCount;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getDisabled() {
		return disabled;
	}

	public void setDisabled(int disabled) {
		this.disabled = disabled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void validate() throws JobConfigurationException {
		if (Strings.isNullOrEmpty(appName)) {
			throw new JobConfigurationException("appName can not be null");
		}

		if (Strings.isNullOrEmpty(jobName)) {
			throw new JobConfigurationException("jobName can not be null");
		}

		if (Strings.isNullOrEmpty(cronExpression) && (repeatIntervalInMs == null)) {
			throw new JobConfigurationException("cronExpression nor repeatIntervalInMs can not be null");
		}
		
		if (Strings.isNullOrEmpty(jobClass)) {
			throw new JobConfigurationException("jobClass can not be null");
		}
	}

}
