package com.github.ghsea.framework.job.common.exception;

public class JobConfigurationException extends JobException {

	private static final long serialVersionUID = -6523547848531319064L;

	public JobConfigurationException(String msg) {
		super(msg);
	}

	public JobConfigurationException(Exception ex) {
		super(ex);
	}
}
