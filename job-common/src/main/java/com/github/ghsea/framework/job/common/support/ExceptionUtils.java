package com.github.ghsea.framework.job.common.support;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
	private static final int MAX_TRACLE_LENGTH = 1000;

	private static final String ELLIPSIS = "...";

	public static String getStackTrace(Throwable throwable) {
		if (throwable == null) {
			return "";
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		String msg = sw.getBuffer().toString();
		if (msg.length() > 1000) {
			msg = msg.subSequence(0, MAX_TRACLE_LENGTH - ELLIPSIS.length()) + ELLIPSIS;
		}
		return msg;
	}
}
