package com.github.ghsea.framework.job.common;

import java.io.Serializable;

public class JobExecutorParams implements Serializable {
	
	private static final long serialVersionUID = 6693591596019873660L;
	private String targetClass;
	
	private String jobUuid;

	public String getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	public String getJobUuid() {
		return jobUuid;
	}

	public void setJobUuid(String jobUuid) {
		this.jobUuid = jobUuid;
	}

}
