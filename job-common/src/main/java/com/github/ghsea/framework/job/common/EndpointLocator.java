package com.github.ghsea.framework.job.common;

public interface EndpointLocator {

	Endpoint select();
}
