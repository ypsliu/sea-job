package com.github.ghsea.framework.job.common.exception;

public class JobServerNotExistException extends JobException {

	private static final long serialVersionUID = 4372554601137606976L;

	public JobServerNotExistException(String msg) {
		super(msg);
	}

	public JobServerNotExistException(Exception ex) {
		super(ex);
	}
}
