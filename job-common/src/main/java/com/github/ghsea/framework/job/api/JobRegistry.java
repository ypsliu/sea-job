package com.github.ghsea.framework.job.api;

import com.github.ghsea.framework.job.common.AuthorityInfo;
import com.github.ghsea.framework.job.common.TriggerMetadata;
import com.github.ghsea.framework.job.common.exception.JobRegistryException;

/**
 * 用于客户端向服务端注册Job
 * 
 * @author Administrator
 *
 */
public interface JobRegistry {
	void register(AuthorityInfo authority, TriggerMetadata trigger) throws JobRegistryException;
}
