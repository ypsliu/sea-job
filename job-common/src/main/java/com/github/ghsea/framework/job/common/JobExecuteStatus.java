package com.github.ghsea.framework.job.common;

public enum JobExecuteStatus {

	OK_SENT(100, "成功派送"),

	OK_EXECUTING(101, "执行中"),
	
	OK_EXECUTED(102, "执行成功"),

	ERROR_FAILED_SEND(900, "送派送失败"),

	ERROR_EXETUTED(901, "执行失败");

	private int code;

	private String description;

	JobExecuteStatus(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}
