package com.github.ghsea.framework.job.common.exception;

public class JobRegistryException extends JobException {
	
	private static final long serialVersionUID = 736860224526007368L;

	public JobRegistryException(String msg) {
		super(msg);
	}

	public JobRegistryException(Exception ex) {
		super(ex);
	}
}
