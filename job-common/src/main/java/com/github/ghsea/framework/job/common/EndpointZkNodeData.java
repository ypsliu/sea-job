package com.github.ghsea.framework.job.common;

import java.io.Serializable;

public class EndpointZkNodeData implements Serializable {
	
	private static final long serialVersionUID = -7551264871940228981L;
	private String baseAccessUrl;

	public String getBaseAccessUrl() {
		return baseAccessUrl;
	}

	public void setBaseAccessUrl(String baseAccessUrl) {
		this.baseAccessUrl = baseAccessUrl;
	}

}
