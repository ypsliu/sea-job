package com.github.ghsea.framework.job.common;

import java.io.Serializable;

public class JobExecuteResult implements Serializable {

	private static final long serialVersionUID = 5591135460603577743L;

	private String appName;

	private String jobName;

	private String jobUuid;

	/**
	 * 成功 OR 失败
	 */
	private boolean ok = true;

	/**
	 * <code>JobExecuteStatus</code>
	 */
	private Integer status;

	private String exception;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getJobUuid() {
		return jobUuid;
	}

	public void setJobUuid(String jobUuid) {
		this.jobUuid = jobUuid;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Override
	public String toString() {
		return "JobExecuteResult [appName=" + appName + ", jobName=" + jobName + ", jobUuid=" + jobUuid + ", ok=" + ok
				+ ", status=" + status + ", exception=" + exception + "]";
	}

}
