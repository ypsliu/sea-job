package com.github.ghsea.framework.job.common;

import java.util.List;

import com.google.common.base.Splitter;

public class Endpoint {
	private String ip;

	private int port;

	private String baseAccessUrl;

	public Endpoint(String ip, int port, String baseUrl) {
		this.ip = ip;
		this.port = port;
		this.baseAccessUrl = baseUrl;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	@Override
	public int hashCode() {
		return ip.hashCode() + port;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Endpoint)) {
			return false;
		}

		Endpoint otherKey = (Endpoint) other;
		return otherKey.getIp().equals(this.getIp()) && otherKey.getPort() == this.getPort();
	}

	@Override
	public String toString() {
		return "JobServerKey [ip=" + ip + ", port=" + port + "]";
	}

	public static Endpoint build(String zkNode, String baseAccessUrl) {
		List<String> ipAndPort = Splitter.on(":").trimResults().splitToList(zkNode);
		return new Endpoint(ipAndPort.get(0), Integer.parseInt(ipAndPort.get(1)), baseAccessUrl);
	}

	public String getBaseAccessUrl() {
		return baseAccessUrl;
	}

}
