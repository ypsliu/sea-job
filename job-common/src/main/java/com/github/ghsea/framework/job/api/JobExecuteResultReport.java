package com.github.ghsea.framework.job.api;

import com.github.ghsea.framework.job.common.JobExecuteResult;

public interface JobExecuteResultReport {

	void report(JobExecuteResult ret);
}
