package com.github.ghsea.framework.job.api;

import com.github.ghsea.framework.job.common.JobExecuteResult;
import com.github.ghsea.framework.job.common.JobExecutorParams;

public interface JobExecutor {

	JobExecuteResult execute(JobExecutorParams params);
}
