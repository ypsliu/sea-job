package com.github.ghsea.framework.job.common;

import java.util.List;

public class JobApp {

	/**
	 * hession context
	 */
	private String baseAccessUrl;
	
	private AuthorityInfo authorityInfo;
	
	private int port;

	private List<TriggerMetadata> triggers;

	public String getBaseAccessUrl() {
		return baseAccessUrl;
	}

	public void setBaseAccessUrl(String baseAccessUrl) {
		this.baseAccessUrl = baseAccessUrl;
	}

	public List<TriggerMetadata> getTriggers() {
		return triggers;
	}

	public void setTriggers(List<TriggerMetadata> triggers) {
		this.triggers = triggers;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public AuthorityInfo getAuthorityInfo() {
		return authorityInfo;
	}

	public void setAuthorityInfo(AuthorityInfo authorityInfo) {
		this.authorityInfo = authorityInfo;
	}

}
