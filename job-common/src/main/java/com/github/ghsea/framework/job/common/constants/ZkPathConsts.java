package com.github.ghsea.framework.job.common.constants;

public class ZkPathConsts {

	public static final String ZK_PATH_ROOT = "/sea-job";

	/// sea-job/clients
	public static final String ZK_PATH_CLIENT = "/clients";

	private ZkPathConsts() {

	}

}
