package com.github.ghsea.framework.job.common;

import java.io.Serializable;

/**
 * 权限验证
 * 
 * @author Administrator
 *
 */
public class AuthorityInfo implements Serializable {

	private static final long serialVersionUID = -5282848073354290779L;

	private String appName;

	private String accessKey;

	private String accessPassword;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getAccessPassword() {
		return accessPassword;
	}

	public void setAccessPassword(String accessPassword) {
		this.accessPassword = accessPassword;
	}

}
