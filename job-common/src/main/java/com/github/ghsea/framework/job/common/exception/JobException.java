package com.github.ghsea.framework.job.common.exception;

public class JobException extends Exception {
	
	private static final long serialVersionUID = 488489425708782504L;

	public JobException(String msg) {
		super(msg);
	}

	public JobException(Exception ex) {
		super(ex);
	}
}
