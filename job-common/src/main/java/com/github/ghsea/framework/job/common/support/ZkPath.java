package com.github.ghsea.framework.job.common.support;

import org.apache.curator.utils.ZKPaths;

public class ZkPath {

	public static final String ZK_PATH_ROOT = "/sea-job";

	public static final String ZK_PATH_SERVER = "/servers";

	public static final String ZK_PATH_CLIENT = "/clients";

	private ZkPath() {

	}

	/**
	 * /sea-job/servers
	 * 
	 * @return
	 */
	public static String getPathOfServers() {
		return ZKPaths.makePath(ZK_PATH_ROOT, ZK_PATH_SERVER);
	}

	public static String getPathOfClients() {
		return ZKPaths.makePath(ZK_PATH_ROOT, ZK_PATH_CLIENT);
	}

}
