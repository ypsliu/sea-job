package com.github.ghsea.framework.job.client;

public interface Job {

	void execute(JobExecutionContext exeCtx);
}
