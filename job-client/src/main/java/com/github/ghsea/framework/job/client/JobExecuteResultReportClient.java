package com.github.ghsea.framework.job.client;

import java.net.MalformedURLException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.github.ghsea.framework.job.api.JobExecuteResultReport;
import com.github.ghsea.framework.job.common.Endpoint;
import com.github.ghsea.framework.job.common.JobExecuteResult;

/**
 * TODO ProxyFactory
 * @author Administrator
 *
 */
public class JobExecuteResultReportClient implements JobExecuteResultReport {

	private JobServerLocator serverLocator;

	private HessianProxyFactory factory = new HessianProxyFactory();

	JobExecuteResultReportClient(JobServerLocator serverLocator) {
		this.serverLocator = serverLocator;
	}

	@Override
	public void report(JobExecuteResult exeRet) {
		// TODO readtimeout,collection timeout ,retry.
		Endpoint serverEndPoint = serverLocator.select();
		String serverUrl = "http://" + serverEndPoint.getIp() + ":" + serverEndPoint.getPort()
				+ serverEndPoint.getBaseAccessUrl() + "/jobExecuteResultReport";
		try {
			JobExecuteResultReport jobReport = (JobExecuteResultReport) factory.create(JobExecuteResultReport.class, serverUrl);
			jobReport.report(exeRet);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
