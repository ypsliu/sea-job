package com.github.ghsea.framework.job.client;

import com.github.ghsea.framework.job.common.JobExecuteResult;

public interface JobExecuteHandler {
	JobExecuteResult handle(Job job, JobExecutionContext ctx);
}
