package com.github.ghsea.framework.job.client;

public class JobExecutionContext {

	private String taskUuid;

	public String getTaskUuid() {
		return taskUuid;
	}

	public void setTaskUuid(String taskUuid) {
		this.taskUuid = taskUuid;
	}

}
