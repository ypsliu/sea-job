package com.github.ghsea.framework.job.client;

import java.util.ArrayList;
import java.util.List;

import org.apache.curator.utils.ZKPaths;

import com.github.ghsea.framework.job.common.AbstractEndpointLocator;
import com.github.ghsea.framework.job.common.Endpoint;
import com.github.ghsea.framework.job.common.EndpointChangedListener;
import com.github.ghsea.framework.job.common.EndpointZkNodeData;
import com.github.ghsea.framework.job.common.exception.JobServerNotExistException;
import com.github.ghsea.framework.job.common.support.ZkPath;
import com.github.ghsea.framework.zkclient.ZkClient;

public class JobServerLocator extends AbstractEndpointLocator {

	private ZkClient zkClient;

	private EndpointChangedListener endpointListener;

	public JobServerLocator(ZkClient zkClient) {
		super();
		this.zkClient = zkClient;
		endpointListener = new EndpointChangedListener();
		init();
	}

	private void init() {
		String jobServerPath = ZkPath.getPathOfServers();
		try {
			endpointListener = new EndpointChangedListener();
			List<String> serverPaths = zkClient.getChildrenAndWatch(jobServerPath, endpointListener);

			if (serverPaths == null || serverPaths.size() == 0) {
				throw new JobServerNotExistException("There is no job server in the Zookeeper servers.");
			}

			List<Endpoint> endpoints = new ArrayList<>(serverPaths.size());
			serverPaths.forEach(each -> {
				EndpointZkNodeData data;
				try {
					data = (EndpointZkNodeData) zkClient.getData(ZKPaths.makePath(jobServerPath, each));
					endpoints.add(Endpoint.build(each, data.getBaseAccessUrl()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			endpointListener.init(endpoints);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected List<Endpoint> listAll() {
		return endpointListener.all();
	}

}
